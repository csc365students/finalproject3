package edu.missouristate.controllers;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.missouristate.model.Example;
import edu.missouristate.model.Players;
import edu.missouristate.services.FinalProjectService2;

@Controller


public class FinalProjectContoller {
	@Autowired
	FinalProjectService2 finalProjectService;

	@ResponseBody 
	@GetMapping(value = { "/finalprojectstring"}) 
	public String getExample(Model model) throws Exception {
		return "Hello World!";
	}
	
	@GetMapping("/finalproject")
	public String getExampleTable(HttpServletResponse reponse, Model model, HttpServletRequest request, HttpSession session) {
		model.addAttribute("message", "HI!!!");
            return "finalproject";
	}
	
	@GetMapping("/FinalProject3")
	public String getPlayers(HttpServletResponse response, Model model, HttpServletRequest request, HttpSession session) {
		List<Players> playersList =finalProjectService.getPlayers();
		 model.addAttribute("playerslist", playersList);
            return "finalproject3";
	}
}
 