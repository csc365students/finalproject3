package edu.missouristate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.model.Players;
import edu.missouristate.util.MSU;

//import com.oreillyauto.dao.custom.ExampleRepositoryCustom;
//import com.oreillyauto.domain.examples.Example;

@Repository
public class FinalProjectRepository2 {
	
	@Autowired
	JdbcTemplate template;
	
    public FinalProjectRepository2() {
    }    
    
	/**public List<Players> getPlayers() {
		String sql = "SELECT * " + 
	                 "  FROM players ";
		//Object[] args = null;
		List<Map<String, Object>> resultSet = template.queryForList(sql);
		List<Players> playersList = new ArrayList<Players>();
		
		for (Map<String, Object> map : resultSet) {
			Players players = new Players();
			
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				String key = entry.getKey();
				String data = ((entry.getValue() == null) ? null : entry.getValue().toString());

				switch(key) {
				case "ID":					
					players.setId(Integer.valueOf(data));
					break;
				case "FIRST_NAME":
					players.setFirstName(data);
					break;
				case "LAST_NAME":
					players.setLastName(data);
					break;
				case "Number":
					players.setNumber(Integer.valueOf(data));
					break;
				case "Sport_Played":
					players.setSportPlayed(data);
					break;
				}
			}
			
			if (players.getId() != null) {
				playersList.add(players);
			}
		
		}
		
		
		
		return playersList;
	}*/
    public List<Players> getPlayers() {
		String sql = "SELECT * " + 
	                 "  FROM players ";
		
		Object[] args = null;		
		List<Players> playerList = template.query(sql, MSU.WIDGET_BPRM);		
		return playerList;
	}
    
    public void printList(List<?> list) {
        for (Object result : list) {
            System.out.println(result);
        }
    }



}

