package edu.missouristate.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.missouristate.dao.FinalProjectRepository2;
import edu.missouristate.model.Players;
import edu.missouristate.services.FinalProjectService2;






@Service("finalProjectService")
public class FinalProjectServiceImpl implements FinalProjectService2 {
    
    @Autowired
    FinalProjectRepository2 finalProjectRepo;

	

	@Override
	public List<Players> getPlayers() {
	    return finalProjectRepo.getPlayers();
		
	}
   
}
