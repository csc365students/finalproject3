CREATE TABLE players (
	id INTEGER GENERATED ALWAYS AS IDENTITY,
	first_name VARCHAR(32) NOT NULL,
	last_name VARCHAR(32) NOT NULL,
	number INTEGER NOT NULL,
	sport_played VARCHAR(32) NOT NULL,
	
	PRIMARY KEY(id)
);

INSERT INTO players (first_name, last_name, number, sport_played)
VALUES
 ('Tom', 'Brady', 12, 'football')
,('Lionell', 'Messi', 10, 'soccer')
,('TJ', 'Oshie', 74, 'hockey');