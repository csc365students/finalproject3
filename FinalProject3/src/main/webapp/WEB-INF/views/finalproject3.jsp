<!doctype html>
<html lang="en">
<%@ include file="/WEB-INF/layouts/include.jsp" %>
<%@ include file="/WEB-INF/layouts/head.jsp" %>
<body id="demo-body">
	<div id="demo-main-div" class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<h1>Players Page</h1>
				<c:if test = "${empty playerslist}">
					No Records
				</c:if>
				<table class= "table table-bordered table=hover">
				<tr> 
				    <th> ID </th>
					<th> First Name </th>
					<th> Last Name </th>
					<th> Number </th>
					<th> Sport Played </th>
				</tr>
				
				<c:if test = "${not empty playerslist}">
				<c:forEach var="players" items="${playerslist}">
				<tr>
				    <td> ${players.id}</td>
				    <td> ${players.firstName}</td>
				    <td> ${players.lastName}</td>
				    <td> ${players.number}</td>
				    <td> ${players.sportPlayed}</td>
				    </tr>
				</c:forEach>
				</c:if>
			</table>
				
				</div>
		</div>
	</div>
</body>
</html>